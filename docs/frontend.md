# 3. Front-End & Platform

**Web dashboard**

The dashboard is a simple React app, connecting to the mosquitto MQTT server and showing each beacon on screen.

The browser window will automatically open the dashboard react app, just go to http://localhost:3000/ (or you can just tick the adress of the npm server that runs on the raspberry pi)

or just open it from the mobile app, at the 3rd menu: MAP, on the resort map (Location Tracking).

![Localhost](img/tr/loctrack.jpeg)

Stations and beacons will show up on our map when the three ESP32 stations are connected. *(Picture 1 in 3.5)*


### Making the app

One of the most important things that any IOT project requires is a database to store the values, results and do some computation on them.

For our project we are using google spreadsheet to store and collect data coming from the mobile app. 
Our mobile app support Android and Apple devices. The environment that we are using is glide.
You can install the app by scanning the QRcode or by following the link: [TRIBE](https://tribe-innov8rs.glideapp.io/)

![qrcode](img/tr/qrcode.jpg)

This will open the app and you can than register yourself or your tribe (group).

We are linking the app with the localhost, which is running from the RaspberryPi. And because we have made the literation with the ESP32 beacons, we can see all the people that are in the area of the beacons. Determaning the location of everyone inside the radius of the literation (with coordinates calculations).**(Picture 2 + 2.1 in 3.5)**

There is also the ability to pay with mope. **(Picture 3 in 3.5)**

## 3.1 Objectives

* The network of BLE beacons (bleutooth low energy) were specifically chosen so that the app still works and still gets notifications even when not connected to the internet. 

* We need to figure out how to connect the interactive map to the app. This gets done bij just linking the page of the map to the app.

* The visitor and resort need to stay in contact with each other (interaction). 

* The layout had to be easy to navigate in order not to confuse the user. We made sure of keeping everything simple. 


## 3.3 Steps taken

**The mobile app**

![start](img/tr/start.jpg)

We could make our platform with add-ons on a blank canvas to work on. 

We edited:

- The layout

- The logo's

- The add ons like easy booking, being able to pay with mope, the user seeing their location, being able to communicste with the resort.
Letting the user be able to make a tribe to register themselves and their group.

- Linking everything together

![app1](img/tr/app1.jpg)

![app2](img/tr/app2.jpg)

*Putting al the bars and oppitions and changing the layout*


![sheets](img/tr/sheets.jpeg)

*The data from the google sheets gives information to the mobile app and also stores all of the user info*

## 3.4 Testing & Problems

- Our web dashboard had to be hardcoded to be able to find the right ip adress. We put our heads together and did it and got to see ourselves on the map.

- The name of the html page needed to be changed. We got that done.

- We have to be able to give the beacons one position and name them. We were able to name them and see how they positioned themselves within the interactive map. 

- After registering the user has to be able to see themselve within the map but should have different colors especially when in a tribe.

- The network ip also changes wherever we go, so when we are going to demo we need to make sure we know the right ip.

## 3.5 Proof of work

The link to our mobile app [TRIBE](https://tribe-innov8rs.glideapp.io/)

![map](img/tr/map.jpg)
*Picture 1 the stations are the people and the beacons are the circles*

![start](img/tr/start.jpg)
*Picture 2*

![map+](../img/tr/map+.jpg)
*Picture 2.1*

![pay1](img/tr/pay1.jpg)
*Picture 3 pay with mope*

[Download Mobile app walkthrough here](files/appvid.rar)

## 3.6 Files & Code

[Download our FRONT END code here](files/dashboard.rar)

## 3.7 References & Credits

For the BLE beacon network positioning [Simon Bogh indoor positioning](https://github.com/simonbogh/ESP32-iBeacon-indoor-positioning)

For the app [Glide](https://www.glideapps.com/templates)

