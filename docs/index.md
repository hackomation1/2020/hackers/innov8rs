# 1. Project Title: TRIBE

![logo](img/tr/logo.jpg)

Our project provides the resort or bussines with a platform of add-ons that include a dashboard with the view of visitor density and real-time location tracking. 
And the visitor gets an mobile app with an interactive map.

## Overview & Features

Our platform has the ability for add-ons like:
*  Book
*  Pay
*  Free/busy
*  Activity calender

One of the most important things that any IOT project requires is a database to store the values, results and do some computation on them.
For our project we are using google spreadsheet to store and collect data to build the interactive mobile app on Android and Apple devices. 
The environment that we are using is glide.

## Demo / Proof of work

### Presentation Pitch 

<iframe width="560" height="315" src="https://www.youtube.com/embed/DI8KQ_GUUbI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Mobile app TRIBE

<iframe width="560" height="315" src="https://www.youtube.com/embed/qG8slYq_QcI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


#### Video Literation of the Beacons

<iframe width="560" height="315" src="https://www.youtube.com/embed/Nlko1_aC3ko" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**All pictures in the documentation are ones that we made of our product**

## Deliverables

**Tribe Mobile Page**

![map](img/tr/page.PNG)

**Interactive map**

![map](img/tr/map+.jpg)

**Mobile app**

![start](img/tr/start.jpg)


- Product pitch deck

[TRIBE pitch deck](https://docs.google.com/presentation/d/1H-FIwNTkSwomOy7MMpMZessw0HpeiyWvzj6HpUgi5fA/edit?usp=sharing)


## Project landing page/website (optional)

Our project can be visited with a link:[TRIBE](https://tribe-innov8rs.glideapp.io/) or with a QRcode

![qrcode](img/tr/qrcode.jpg)


## The INNOV8RS

![inno](img/tr/inno.png)

Contact us at: innov8rssu@gmail.com

![This is us](img/tr/us.jpeg)

### Ivy Amatredjo

[Read about Ivy here](https://codettes-boothcamp.gitlab.io/students/ivy-amatredjo); 
Contact: ivy.amatredjo@gmail.com (8790163)


### Shofia Notoesowito

[Read about Shofia here](https://codettes-boothcamp.gitlab.io/students/shofia-notosoewito); 
Contact: shofianoto@gmail.com (8768496)

### Faith Pherai

[Read about Faith here](https://codettes-boothcamp.gitlab.io/students/faith-pherai); 
Contact: angelipherai@gmail.com (8639690)

While making TRIBE we did everything together. From troubleshooting to figuring out how to make things work.
When one came with an idea we all stepped in and gave a bit of our own knowledge to help.
We are a GREAT TEAM after all!!

