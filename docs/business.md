# 4. Business & Marketing

<Describe the embedded solution for your project here.>

Suriname is a beautiful country with a lot of nice resorts, places, hotspots, big hotels where people like to go, but all of them have one common problem, they all lack Guidance.
Most of the time visitors don't know how to get fimiliar with the place, because they don't have the slidest idea what these places have to offer. 
So we give the service to these resorts owners an interactive map, where visitors can find themself in the map and can see which activities (sauna, pool, bar etc) or events (stages with different kinds of bands/Dj's etc) there are on the map.
Every resort must have their own map, which can be uploaded in our system. 

## 4.1 Objectives

<Provide background and reasoning what needs to be solved here, why this solution was choosen and what steps need to be taken>

e.g. of an user expierence

- clueless: Visitors are in a resort, they arrive there, but have no clue where to go or how to start.
    - visitors need a map to show them where to go and whats on the resort
- Register: Visitors have to go to the lobby, asking where are where, but forget it after an hour
    - visitors got to register online with a mobile app (scanning a QRCode or visit the link)
- Book: it's mendatory to book at the lobby for every event/activity at the resort
    - visitors have the ability to book and pay online, even from there room or dinning table
- Events/activity availability: what activities do the resort have and what time is it available (kitchen opens at etc )
    - Activity calender available checking the availablity and schedule themself
- Interaction with the resort
    - The resort can sent visitors a bradcast message
- visitor tracking: where are they, which activity is the most visited one
    - Visitors can see themself on the interactive map


## Our video

### Mobile app TRIBE

<iframe width="560" height="315" src="https://www.youtube.com/embed/qG8slYq_QcI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Interactive map for every resort their own

<iframe width="480" height="299" src="https://www.youtube.com/embed/FCJzs4lS9yc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## 4.3 Ananlyze your market & segments

<Identify Customer segments, value proposition, channels and identify monitizable pains>

### TARGET CUSTOMER
- RESORT OWNERS
- HOTEL OWNERS
- ALL PLACES WITH POTENTIAL VISITORS

### CUSTOMER CHANNEL
- MOBILE APP: Data gathering (e-mails), which we can use to send Promo's
- SOCIAL MEDIA
- PROMOTIONS

### REVENUE STREAM
- Consult resorts to digitize their customer experience (design a beacon network for each park)
- Building & Setting up interactive maps (graphics) 
- Service & Maintenance fees
- Selling Tribe Add-ons (booking, pay, free/busy, loyalty .. etc)
- HARDWARE SALES

### Monitizable pains
- NETWORKING
- INFRASTRUCTURE
- TRACKING SYSTEM
- INTERACTION
- WEBSOCKET


Cost:

![Cost](img/tr/cost.PNG)

## 4.4 Build your business canvas

https://canvanizer.com/canvas/w2PxuimRQG8CV

## 2.5 Build your pitch deck

https://docs.google.com/presentation/d/1H-FIwNTkSwomOy7MMpMZessw0HpeiyWvzj6HpUgi5fA/edit#slide=id.gadc2eae3ff_2_213

## 4.6 Make a project Poster

<Build a poster for your project describing in a nutshell what it is for expo purposes>

![Tribe Product Poster](img/tr/poster.jpeg)

## 4.7 Files & Code

<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

For the BLE beacons using the ESP32

```
#include "sys/time.h"
#include "BLEDevice.h"
#include "BLEUtils.h"
#include "BLEServer.h"
#include "BLEBeacon.h"
#include "esp_sleep.h"

#define GPIO_DEEP_SLEEP_DURATION     10  // sleep x seconds and then wake up
RTC_DATA_ATTR static time_t last;        // remember last boot in RTC Memory
RTC_DATA_ATTR static uint32_t bootcount; // remember number of boots in RTC Memory

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
BLEAdvertising *pAdvertising;   // BLE Advertisement type
struct timeval now;

#define BEACON_UUID "87b99b2c-90fd-11e9-bc42-526af7764f64" // UUID 1 128-Bit (may use linux tool uuidgen or random numbers via https://www.uuidgenerator.net/)

void setBeacon() {

  BLEBeacon oBeacon = BLEBeacon();
  oBeacon.setManufacturerId(0x4C00); // fake Apple 0x004C LSB (ENDIAN_CHANGE_U16!)
  oBeacon.setProximityUUID(BLEUUID(BEACON_UUID));
  oBeacon.setMajor((bootcount & 0xFFFF0000) >> 16);
  oBeacon.setMinor(bootcount & 0xFFFF);
  BLEAdvertisementData oAdvertisementData = BLEAdvertisementData();
  BLEAdvertisementData oScanResponseData = BLEAdvertisementData();

  oAdvertisementData.setFlags(0x04); // BR_EDR_NOT_SUPPORTED 0x04

  std::string strServiceData = "";

  strServiceData += (char)26;     // Len
  strServiceData += (char)0xFF;   // Type
  strServiceData += oBeacon.getData();
  oAdvertisementData.addData(strServiceData);

  pAdvertising->setAdvertisementData(oAdvertisementData);
  pAdvertising->setScanResponseData(oScanResponseData);
}

void setup() {

  Serial.begin(115200);
  gettimeofday(&now, NULL);
  Serial.printf("start ESP32 %d\n", bootcount++);
  Serial.printf("deep sleep (%lds since last reset, %lds since last boot)\n", now.tv_sec, now.tv_sec - last);
  last = now.tv_sec;

  // Create the BLE Device
  BLEDevice::init("ESP32 as iBeacon");
  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer(); // <-- no longer required to instantiate BLEServer, less flash and ram usage
  pAdvertising = BLEDevice::getAdvertising();
  BLEDevice::startAdvertising();
  setBeacon();
  // Start advertising
  pAdvertising->start();
  Serial.println("Advertizing started...");
  delay(100);
  pAdvertising->stop();
  Serial.printf("enter deep sleep\n");
  esp_deep_sleep(1000000LL * GPIO_DEEP_SLEEP_DURATION);
  Serial.printf("in deep sleep\n");
}

void loop() {
}

```

## 4.8 References & Credits

<add all used references & give credits to those u used code and libraries from>

- https://www.glideapps.com
- https://pubsubclient.knolleary.net/
- https://blog.shinovent.fi/2018/03/ibeacon-indoor-positioning-with-esp32.html
- https://proximi.io/accurate-indoor-positioning-bluetooth-beacons/

