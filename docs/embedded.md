# 2. Embedded solution

<Describe the embedded solution for your project here.>
### Using the ESP32 as a Beacon.

ESP32 is really a powerful IoT device having inbuilt support for Bluetooth and WiFi. The ESP32 is an advanced version of it's predecessor ESP8266 with extra features like RAM, ROM, GPIO Pins etc. The ESP32 module supports both classic Bluetooth and Bluetooth Low Energy (BLE).

![esp32](img/tr/esp32.jpeg)

### Digital Manufacturing

And now we design our own encosure for the ESP32 in tinkercad, 

![Tinkercad](img/tr/tinker.jpeg)


generating the GCode with Cura 

![Cura](img/tr/cura.jpeg)


![Parameters](img/tr/para.jpeg)


and printing it with our 3D printer, the anycube 4 max pro, with a pink PLA fillement, looking like this:

![Enclosure](img/tr/enclose.jpeg)

### Programming ESP32 to work as BLE iBeacon

As we know the BLE can operate in two different modes:
* Server mode
* Client mode

## 2.1 Objectives

<Provide background and reasoning what needs to be solved here, why this solution was choosen and what steps need to be taken>

We have used ESP32 for making our BLE beacons. In order to get the location of the visitors on the map, we have to make a literation with minimal 3 BLE beacons. So in order to let the beacon work, we have to flash them first with the  adruino sketch. Which will put the beacon on the same network as the server which will host the interactive map.

## 2.3 Steps taken

### Steps we have taken are

- We Opened Arduino IDE and Select “ESP32 Dev Module”. (we installed the ESP32 Board Package first, else you could not select the board in the Arduino IDE)
- Go to File > Examples > ESP32 BLE Arduino > BLE_iBeacon
- Open “BLE_iBeacon” Sketch.

![sketch](img/tr/sketch.jpg)

The ESP32 BLE libraries are included which contains many functions used to make ESP32 in different configurations such as BLE Client or BLE Server.

```
#include "BLEDevice.h"
#include "BLEUtils.h"
#include "BLEServer.h"


The whole code that we are using in this case is as followed:


#include "sys/time.h"
#include "BLEDevice.h"
#include "BLEUtils.h"
#include "BLEServer.h"
#include "BLEBeacon.h"
#include "esp_sleep.h"

#define GPIO_DEEP_SLEEP_DURATION     10  // sleep x seconds and then wake up
RTC_DATA_ATTR static time_t last;        // remember last boot in RTC Memory
RTC_DATA_ATTR static uint32_t bootcount; // remember number of boots in RTC Memory

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
BLEAdvertising *pAdvertising;   // BLE Advertisement type
struct timeval now;

#define BEACON_UUID "87b99b2c-90fd-11e9-bc42-526af7764f64" // UUID 1 128-Bit (may use linux tool uuidgen or random numbers via https://www.uuidgenerator.net/)

void setBeacon() {

  BLEBeacon oBeacon = BLEBeacon();
  oBeacon.setManufacturerId(0x4C00); // fake Apple 0x004C LSB (ENDIAN_CHANGE_U16!)
  oBeacon.setProximityUUID(BLEUUID(BEACON_UUID));
  oBeacon.setMajor((bootcount & 0xFFFF0000) >> 16);
  oBeacon.setMinor(bootcount & 0xFFFF);
  BLEAdvertisementData oAdvertisementData = BLEAdvertisementData();
  BLEAdvertisementData oScanResponseData = BLEAdvertisementData();

  oAdvertisementData.setFlags(0x04); // BR_EDR_NOT_SUPPORTED 0x04

  std::string strServiceData = "";

  strServiceData += (char)26;     // Len
  strServiceData += (char)0xFF;   // Type
  strServiceData += oBeacon.getData();
  oAdvertisementData.addData(strServiceData);

  pAdvertising->setAdvertisementData(oAdvertisementData);
  pAdvertising->setScanResponseData(oScanResponseData);
}

void setup() {

  Serial.begin(115200);
  gettimeofday(&now, NULL);
  Serial.printf("start ESP32 %d\n", bootcount++);
  Serial.printf("deep sleep (%lds since last reset, %lds since last boot)\n", now.tv_sec, now.tv_sec - last);
  last = now.tv_sec;

  // Create the BLE Device
  BLEDevice::init("ESP32 as iBeacon");
  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer(); // <-- no longer required to instantiate BLEServer, less flash and ram usage
  pAdvertising = BLEDevice::getAdvertising();
  BLEDevice::startAdvertising();
  setBeacon();
  // Start advertising
  pAdvertising->start();
  Serial.println("Advertizing started...");
  delay(100);
  pAdvertising->stop();
  Serial.printf("enter deep sleep\n");
  esp_deep_sleep(1000000LL * GPIO_DEEP_SLEEP_DURATION);
  Serial.printf("in deep sleep\n");
}

void loop() {
}
```

It's very important to have the right credetials:
As you see, you got to change your SSID en Password and don't forget the MQTTserver.

```
#ifndef CREDENTIALS_H_
#define CREDENTIALS_H_
/* WiFi username and password */
const char* ssid = "fakeSSID";
const char* password = "fakepassword";

/* MQTT credentials and connection */
const char* mqttServer = "192.168.18.22";
const int mqttPort = 1883;
const char* mqttUser = "station";
const char* mqttPassword = "bledemo";

#endif
```

### Installing the Server on our RaspberryPi

We need all three ESP32 BLE beacons in order to run the application, since this is required to do the literation.
We used Nodejs for this project, which have it's own folder structure.

![WINSCP](img/tr/winscp.PNG)

First we installed the Mosquitto MQTT Broker (server) on our project first.

![WINSCP](img/tr/MQTT.jpg)

```
sudo apt install mosquitto
```

NPM for running the web dashboard

```
sudo apt install npm
```

We have installed some extra libraries via NPM in order to see an error if so, when we run the dashboard.
using the following command:

```
npm install chalk
```
When starting the npm server, you will see this:

![WINSCP](img/tr/npmserver.PNG)

We are using the Arduino BLE and MQTT libraries:

- PubSubClient
- ESP32 BLE Arduino

There were problems in sending too much data. So we checked the serial console for an ESP32 and make sure that it prints out PUB Result: 1.


If it prints PUB Result: 0 there may be a problem transmitting the json data to the MQTT server. We fixed this by changing MQTT_MAX_PACKET_SIZE in the PubSubClient library to 2048.

### Setup and running the Mosquitto MQTT Broker (server)

First edit the file mosquitto-demo.conf. Set the path to acl files, like:

```
acl_file /home/w3/Downloads/iBeacon-indoor-positioning-demo/mosquitto_acl.config
password_file /home/w3/Downloads/iBeacon-indoor-positioning-demo/mosquitto_pw.config
```

Go to putty and start the service
Start mosquitto with the following command in the innov8rs folder itself:

```
mosquitto -v -c ./mosquitto-demo.conf
```

Starting up with putty gives you this screen:

![WINSCP](img/tr/musquitto.jpg)


MQTT users are "station" and "dashboard", and all passwords are "bledemo".

## ESP32 stations

Edit the file credentials.h and change wifi ssid and password, and also change mqttServer to the IP of the computer running the mosquitto server.

![arduinocred](img/tr/arduinocred.jpg)


Upload the Arduino sketch ESP32_station.ino to your ESP32.

![uploading](img/tr/uploading.jpeg)


Place the ESP32 modules around the area where you want to do positioning, preferably 10-15 meters appart and also in wifi range.

ESP32 modules will work as iBeacon monitoring stations, reporting all found bluetooth beacons to the MQTT topic, with their MAC address and RSSI.

### Final architecture

![architect](img/tr/architect.jpeg)

### Web dashboard

The dashboard is a simple React app, connecting to the mosquitto MQTT server and showing each beacon on screen.

To run the dashboard from a terminal go to the dashboard folder and run npm start


```
cd dashboard/
npm start
```

The browser window will automatically open the dashboard react app, and go to http://localhost:3000/ (or you can just tick the adress of the server on the raspberry pi)

Stations and beacons will show up on our map when the three ESP32 stations are connected.

![beacontest](img/tr/beacontest.jpeg)


Beacon test with the raspberry pi

![map](img/tr/map.jpg)


The stations are the people and the beacons are the circles


## 2.4 Testing & Problems

<provide tests performed, describe problems encountered and how they were solved (or not)>

The problem that we had, was the network  (wi-fi) on which the server is connected. Which gave us IP problems.
That's why the BLE Beacons couldn't be connected to the server. It didn't recognize the localhost and it's port.
And we couldn't see the beacons on the map and there for no visitors on the map.

But we solved the problem, placing the beacon and the server on the same network and Ip-range.

## 2.5 Proof of work

### Flashed the BLE Beacons

<iframe width="560" height="315" src="https://www.youtube.com/embed/QmC-m_XYajA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Port we use to flash the beacons

<iframe width="560" height="315" src="https://www.youtube.com/embed/B8LUiRTZnOM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Literation of the Beacons

<iframe width="560" height="315" src="https://www.youtube.com/embed/Nlko1_aC3ko" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 2.6 References & Credits

https://www.ijert.org/research/indoor-localization-using-ble-technology-IJERTCONV6IS13063.pdf

